// ----------------------------------------
// test(simply_slide)
// ----------------------------------------
// single click
simply.on('singleClick', function(e) {
	if (e.button === 'up') {
			ajax({
				url: 'http://192.168.11.3:3000/next'
			});
			simply.vibe();
	}if (e.button === 'down') {
			ajax({
				url: 'http://192.168.11.3:3000/prev'
			});
			simply.vibe();
		}
	simply.subtitle('Pressed ' + e.button + '!');
});

// longClick
simply.on('longClick', function(e) {
	simply.scrollable(e.button !== 'select');
	ajax({
		url: 'http://192.168.11.3:3000/quit'
	});
	simply.vibe();
});

// text
simply.text({
title: 'Simply Slide!!!'
}, true);
