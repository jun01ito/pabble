// ----------------------------------------
// test(simply_slide)
// ----------------------------------------
var app = require('express')();
var http = require('http').Server(app);
//// ----------------------------------------
//// var exec = require('child_process').exec; ~v0.11.0 ~なので
//// var execSync = require('child_process').execSync;
//// ----------------------------------------
var execSync = require('execsync');
//// console.log(execSync('ls'));


// ----------------------------------------
// シェルスクリプト
// ----------------------------------------
function sendNext() { execSync('open ppt_next.command'); }
function sendPrev() { execSync('open ppt_prev.command'); }
function sendQuit() { execSync('open ppt_quit.command'); }


// ----------------------------------------
// node.js -> シェルスクリプトを実行
// ----------------------------------------
// http://192.168.11.3:3000/nextにレスポンスがあった!
app.get('/next', function (req, res) {
	sendNext();
	console.log(' next !!! ');
});
// http://192.168.11.3:3000/prevにレスポンスがあった!
app.get('/prev', function (req, res) {
	sendPrev();
	console.log(' prev !!! ');
});
// http://192.168.11.3:3000/quitにレスポンスがあった!
app.get('/quit', function (req, res) {
	sendQuit();
	console.log(' bye !!! ');
});


// ----------------------------------------
// http://localhost listening on *:3000
// ----------------------------------------
http.listen(3000, function(){
	console.log('listening on *:3000');
});
